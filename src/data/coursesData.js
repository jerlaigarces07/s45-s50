const coursesData = [
  {
    id: 'wdc001',
    name: 'PHP - Laravel',
    description:
      'Lorem ipsum dolor sit amet consectetur adipisicing elit. Laborum accusamus temporibus optio fugit voluptatum velit. Dignissimos neque delectus quia doloribus?',
    price: 45000,
    onOffer: true,
  },
  {
    id: 'wdc002',
    name: 'Python - Django',
    description:
      'Lorem ipsum dolor sit amet consectetur adipisicing elit. Laborum accusamus temporibus optio fugit voluptatum velit. Dignissimos neque delectus quia doloribus?',
    price: 50000,
    onOffer: true,
  },
  {
    id: 'wdc003',
    name: 'Javascript - Springboot',
    description:
      'Lorem ipsum dolor sit amet consectetur adipisicing elit. Laborum accusamus temporibus optio fugit voluptatum velit. Dignissimos neque delectus quia doloribus?',
    price: 55000,
    onOffer: true,
  },
];

export default coursesData;
