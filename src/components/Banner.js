// import Button from 'react-bootstrap/Button';
// Bootstrap frid system components
// import Row from 'react-bootstrap/Row';
// import Column from 'react-bootstrap/Column';
// import { Row } from 'react-bootstrap';
// import { Column } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import { Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner({ data }) {
  const history = useHistory();
  const { title, content, destination, label } = data;

  function handleClick() {
    history.push('/');
  }

  return (
    <Row>
      <Col className="p-5">
        <h1>{title}</h1>
        <p>{content}</p>
        <Button variant="primary" onClick={handleClick}>
          {label}
        </Button>
      </Col>
    </Row>
  );
}
