import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Redirect } from 'react-router';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Register() {
  const { user, setUser } = useContext(UserContext);

  // State hooks to store the values of the input fields
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [mobileNo, setMobileNo] = useState('');
  const [password1, setPassword1] = useState('');
  const [password2, setPassword2] = useState('');

  //   State to determine wheter the submit button is enable or not
  const [isActive, setIsActive] = useState(false);

  // console.log(email);
  // console.log(password1);
  // console.log(password2);

  //   Function to simulate user registration
  function registerUser(e) {
    e.preventDefault();

    fetch('http://localhost:4000/users/register', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        firstName: firstName,
        lastName: lastName,
        email: email,
        mobileNo: mobileNo,
        password: password1,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log('data', data);
        if (typeof data.message === 'undefined') {
          localStorage.setItem('token', data.access);
          retrieveUserDetails(data.access);

          Swal.fire({
            icon: 'success',
            title: 'Successfully Registered',
            text: 'Welcome to Zuitt!',
          });
        } else {
          Swal.fire({
            icon: 'error',
            title: 'Duplicate email found',
            text: 'Please provide a different email',
          });
        }
      })
      .catch((err) => {
        console.log('Error', err);
      });

    setFirstName('');
    setLastName('');
    setEmail('');
    setMobileNo('');
    setPassword1('');
    setPassword2('');
  }

  const retrieveUserDetails = (token) => {
    fetch('http://localhost:4000/users/details', {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
        });
      });
  };

  //   Validate to enable submit button when all fields are populated and both passwords match
  useEffect(() => {
    if (
      email !== 'null' &&
      password1 !== '' &&
      password2 !== '' &&
      password1 === password2
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password1, password2]);

  return user.id != null ? (
    <Redirect to="/courses" />
  ) : (
    <Form onSubmit={(e) => registerUser(e)}>
      {/* Bind the input states via 2-way binding */}
      <Form.Group controlId="userFirstName">
        <Form.Label>First Name</Form.Label>
        <Form.Control
          type="text"
          value={firstName}
          onChange={(e) => setFirstName(e.target.value)}
          required
        />
      </Form.Group>
      <Form.Group controlId="userLastName">
        <Form.Label>Last Name</Form.Label>
        <Form.Control
          type="text"
          value={lastName}
          onChange={(e) => setLastName(e.target.value)}
          required
        />
      </Form.Group>
      <Form.Group controlId="userEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control
          type="email"
          placeholder="Enter email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          required
        />
        <Form.Text className="text-muted">
          We'll never share your email with anyone else.
        </Form.Text>
      </Form.Group>
      <Form.Group controlId="userMobileNo">
        <Form.Label>Mobile Number</Form.Label>
        <Form.Control
          type="number"
          value={mobileNo}
          onChange={(e) => setMobileNo(e.target.value)}
          required
        />
      </Form.Group>
      <Form.Group controlId="password1">
        <Form.Label>Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Password"
          value={password1}
          onChange={(e) => setPassword1(e.target.value)}
          required
        />
      </Form.Group>
      <Form.Group controlId="password2">
        <Form.Label>Verify Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Verify Password"
          value={password2}
          onChange={(e) => setPassword2(e.target.value)}
          required
        />
      </Form.Group>
      {/* Conditionally render the submit button based on isActive state*/}
      {isActive ? (
        <Button className="mt-3" variant="primary" type="submit" id="submitBtn">
          Submit
        </Button>
      ) : (
        <Button
          className="mt-3"
          variant="danger"
          type="submit"
          id="submitBtn"
          disabled
        >
          Submit
        </Button>
      )}
    </Form>
  );
}
